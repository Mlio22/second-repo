from django.urls import path, re_path, include
from .views import index, message_post, message_table

app_name = 'guestBook'

urlpatterns = [
    re_path(r'^$', index, name="index"),
    re_path(r'^add_message', message_post, name='add_message'),
    re_path(r'^result_table', message_table, name='result_table'),
]
